﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Platform
{
	public string difficulty;
	public GameObject[] platformPrefab;
}

public class PlatformManager : MonoBehaviour
{

	#region PUBLIC_VARIABLES

	public Platform[] platformPrefabs;
	public PlayerMovement player;
	public string platformSequence;
	public float prefabScale;

	#endregion

	#region PRIVATE_VARIABLES

	private GameObject[] currentPlatform;
	private int sequenceIndex = 0;
	private System.Random random;

	#endregion

	#region UNITY_CALLBACKS

	void Awake ()
	{
		random = new System.Random ();
		currentPlatform = new GameObject[3];
	}

	void Start ()
	{
		//OnGameStart ();
		//InitPlatforms ();
	}

	void OnEnable()
	{
		EventManager.onGameStart += OnGameStart;
		EventManager.onGameSet += InitPlatforms;
	}

	void OnDisable()
	{
		EventManager.onGameStart -= OnGameStart;
		EventManager.onGameSet -= InitPlatforms;
	}

	void Update ()
	{
		if (GameManager.Instance.gameState == GameState.NONE)
			return;

		if (player.transform.position.z > currentPlatform [1].transform.position.z) {
			currentPlatform [0].Recycle ();
			currentPlatform [0] = currentPlatform [1];
			currentPlatform [1] = currentPlatform [2];

			currentPlatform [2] = GeneratePlatform ();
			currentPlatform [2].transform.position = GetPosition (1);
			sequenceIndex++;
		}
	}

	#endregion

	#region PUBLIC_FUNCTIONS

	#endregion

	#region PRIVATE_FUNCTIONS

	private void OnGameStart ()
	{
		SetPlayerReference ();
	}

	void SetPlayerReference ()
	{
		player = PlayerManager.Instance.currentPlayer;
	}

	private void InitPlatforms ()
	{
		Debug.Log ("hello");

		currentPlatform [0] = GeneratePlatform ();
		currentPlatform [0].transform.position = GetPosition (-1);
		sequenceIndex++;

		currentPlatform [1] = GeneratePlatform ();
		currentPlatform [1].transform.position = GetPosition (0);
		sequenceIndex++;

		currentPlatform [2] = GeneratePlatform ();
		currentPlatform [2].transform.position = GetPosition (1);
		sequenceIndex++;

		OnGameStart ();
	}

	private GameObject GeneratePlatform ()
	{
		if (GameManager.Instance.gameState == GameState.GAME_PLAY) {
			if (platformSequence [sequenceIndex] == '1') {

				return ObjectPool.Spawn (platformPrefabs [0].platformPrefab [random.Next (platformPrefabs [0].platformPrefab.Length)], transform, Vector3.zero, Quaternion.identity);
			
			} else if (platformSequence [sequenceIndex] == '2') {
			
				return ObjectPool.Spawn (platformPrefabs [1].platformPrefab [random.Next (platformPrefabs [1].platformPrefab.Length)], transform, Vector3.zero, Quaternion.identity);

			} else if (platformSequence [sequenceIndex] == '3') {

				return ObjectPool.Spawn (platformPrefabs [2].platformPrefab [random.Next (platformPrefabs [2].platformPrefab.Length)], transform, Vector3.zero, Quaternion.identity);

			}
			
		} else if(GameManager.Instance.gameState == GameState.GAME_SET){
			return ObjectPool.Spawn (platformPrefabs [3].platformPrefab [random.Next (platformPrefabs [3].platformPrefab.Length)], transform, Vector3.zero, Quaternion.identity);
		}

		return null;
	}

	private Vector3 GetPosition (int lastPositionIndex)
	{
		if (lastPositionIndex == -1)
			return Vector3.up * platformPrefabs [0].platformPrefab [0].transform.position.y + Vector3.forward * prefabScale;
		
		return currentPlatform [lastPositionIndex].transform.position + Vector3.forward * (prefabScale * 10);
	}

	#endregion

	#region COROUTINE

	#endregion

}
