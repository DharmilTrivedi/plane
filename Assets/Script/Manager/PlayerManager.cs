﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour {

	#region PUBLIC_REFERANCES

	public static PlayerManager Instance;

	public Vector3 startingPosition;
	public PlayerMovement[] playerPrefab;
	public PlayerMovement currentPlayer;
	public int playerIndex;

	#endregion

	#region PRIVATE_VARIABLES
	#endregion

	#region UNITY_CALLBACKS

	void Start()
	{
		Instance = this;


	}

	void OnEnable(){
		EventManager.onGameStart += GeneratePlayer;

		GeneratePlayer ();
		currentPlayer.transform.localPosition = startingPosition;
	}

	void OnDisable(){
		EventManager.onGameStart -= GeneratePlayer;
	}

	#endregion

	#region PUBLIC_METHODS

	public void GeneratePlayer()
	{
		currentPlayer = Instantiate (playerPrefab[playerIndex], startingPosition, Quaternion.identity);
		currentPlayer.transform.SetParent (CameraManager.Instance.transform);
	}

	#endregion

	#region PRIVATE_METHODS
	
	#endregion

	#region COROUTINES
	#endregion
}
