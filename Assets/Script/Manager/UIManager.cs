﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour {

	#region PUBLIC_REFERANCES

	public static UIManager Instance;

	public SplashView splashView;
	public HomeView homeView;
	public GamePlayView gamePlayView;
	public GameEndView gameEndView;

	#endregion

	#region PRIVATE_REFERANCES
	#endregion

	#region UNITY_CALLBACKS

	void Awake(){
		Instance = this;
	//	splashView.ShowView ();
	}

	#endregion

	#region PUBLIC_METHODS
	#endregion

	#region PRIVATE_METHODS
	#endregion

	#region COROUTINES
	#endregion
}
