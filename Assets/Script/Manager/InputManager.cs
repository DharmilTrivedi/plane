﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour 
{
	public static InputManager Instance;
	public bool hasInput;
	public Vector3 input;

	void Awake()
	{
		Instance = this;
	}

	void Update () {
		
		if (Input.GetMouseButton (0)) {
			input = Input.mousePosition;
			hasInput = true;

		} else {
			input = Vector3.zero;
			hasInput = false;
		}
	}
}
