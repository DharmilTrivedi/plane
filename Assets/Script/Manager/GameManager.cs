﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

	#region PUBLIC_REFERNCES

	public static GameManager Instance;
	public GameState gameState;
	#endregion

	#region PRIVATE_VARIABLES
	#endregion

	#region UNITY_CALLBACKS

	void Awake()
	{
		Instance = this;
		gameState = GameState.NONE;
	}

	void Start(){
		//UIManager.Instance.homeView.ShowView ();
	}



	#endregion

	#region PUBLIC_METHODS
	public void OnGameStart()
	{
		EventManager.RaisOnGameStart ();
		gameState = GameState.GAME_PLAY;
	}
	#endregion

	#region PRIVATE_METHODS
	#endregion
}
