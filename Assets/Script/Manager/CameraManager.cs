﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour {

	#region PUBLIC_VARIABLES
	public static CameraManager Instance;
	public Transform target;
	public float speed;
	#endregion

	#region PRIVATE_VARIABLES
	private Vector3 targetPosition;
	#endregion

	#region UNITY_CALLBACKS

	void Awake()
	{
		Instance = this;
	}

	void LateUpdate()
	{
		//SetTargetPosition ();
		//transform.position = targetPosition;
		SetPosition();
	}
	#endregion

	#region PUBLIC_FUNCTIONS

	#endregion

	#region PRIVATE_FUNCTIONS
	private void SetTargetPosition()
	{
		targetPosition = target.position;
		targetPosition.x = transform.position.x;
		targetPosition.y = transform.position.y;
	}

	private void SetPosition()
	{
		//transform.position = Vector3.Lerp (transform.position, targetPosition, speed * Time.deltaTime);
		transform.position += Vector3.forward * Time.deltaTime * speed;
	}
	#endregion

	#region COROUTINE

	#endregion



}
