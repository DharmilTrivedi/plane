﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventManager : MonoBehaviour {

	#region DELEGATES

	public delegate void OnGameStart();
	public static event OnGameStart onGameStart;

	public delegate void OnGameEnd();
	public static event OnGameEnd onGameEnd;

	public delegate void OnGameSet();
	public static event OnGameSet onGameSet;

	#endregion

	#region DELEGATES_DECLARATION

	public static void RaisOnGameStart(){
		if (onGameStart != null) {
			onGameStart();
		}
	}

	public static void RaisOnGameEnd(){
		if (onGameEnd != null) {
			onGameEnd();
		}
	}

	public static void RaiseOnGameSet()
	{
		if (onGameSet != null)
			onGameSet();
	}

	#endregion

	#region PUBLIC_REFERANCES
	#endregion

	#region PRIVATE_REFERANCES
	#endregion

	#region UNITY_CALLBACKS
	#endregion

	#region PUBLIC_REFERANCES
	#endregion

	#region PRIVATE_REFERANCES
	#endregion

	#region COROUTINE
	#endregion
}
