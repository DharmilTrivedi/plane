﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphericalPlayerMovement : MonoBehaviour
{

	#region PUBLIC_VARIABLES

	[Header ("Plane Float")]
	public float floatingHeight;
	public float floatingSpeed;
	public AnimationCurve planeFloatCurve;

	[Header ("Rotation")]
	public float rotationSpeed;
	public float tiltAngle;

	[Header ("Movement")]
	public float movementSpeed;
	public float forwardSpeed;

	[Header ("Boundry")]
	public Boundry boundry;

	#endregion

	#region PRIVATE_VARIABLES

	//rotation
	private float zRotation;
	private float yRotation;

	private Vector3 movement = Vector3.zero;
	private Vector3 clampPos = Vector3.zero;

	#endregion

	void Start ()
	{
		//clampPos = transform.position;
	}

	void FixedUpdate ()
	{
		movement = transform.position;
		FloatThePlane ();

		Move ();

		SetPosition ();
		SetRotation ();
	}

	void Move ()
	{
		MoveHorizontally ();
	}

	void SetPosition ()
	{
		UpdatePosition ();
		ClampPosition ();
	}

	void FloatThePlane ()
	{
		movement.y = Mathf.Sin (Time.time * floatingSpeed) * floatingHeight + transform.position.y;
	}

	void MoveHorizontally ()
	{
		if (InputManager.Instance.hasInput) {
			if (InputManager.Instance.input.x < Screen.width / 2) {
				MoveOnRight ();	

			} else {
				MoveOnLeft ();	
			}
		} else {
			ResetRotation ();
		}

	}

	public void ResetRotation ()
	{
		zRotation = Mathf.MoveTowards (zRotation, 0, rotationSpeed * Time.deltaTime);
	}

	void UpdatePosition ()
	{
		transform.position = movement;
	}

	void SetRotation ()
	{
		transform.eulerAngles = new Vector3 (0, 180, zRotation);
	}

	void ClampPosition ()
	{
		//clampPos = transform.position;
		//clampPos.x = Mathf.Clamp (transform.position.x, boundry.minX, boundry.maxX);

		zRotation = Mathf.Clamp (zRotation, -tiltAngle, tiltAngle);
		//transform.position = clampPos;
	}

	public void MoveOnRight ()
	{
		movement.x += movementSpeed * Time.deltaTime;
		zRotation += Time.deltaTime * rotationSpeed;
	}

	public void MoveOnLeft ()
	{
		movement.x -= movementSpeed * Time.deltaTime;
		zRotation -= Time.deltaTime * rotationSpeed;
	}

}
