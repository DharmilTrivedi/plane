﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class Boundry{
	public float minX,maxX;
}

public class PlayerMovement : MonoBehaviour {

	#region PUBLIC_VARIABLES
	[Header("Plane Float")]
	public float floatingHeight;
	public float floatingSpeed;

	[Header("Rotation")]
	public float rotationSpeed;
	public float tiltAngle;	

	[Header("Movement")]
	public float movementSpeed;
	public float forwardSpeed;

	[Header("Boundry")]
	public Boundry boundry;
	#endregion

	#region PRIVATE_VARIABLES

	//rotation
	private float zRotation;
	private float yRotation;

	private Vector3 movement = Vector3.zero;
	private Vector3 clampPos = Vector3.zero;

	#endregion

	void Start () 
	{
		clampPos = transform.position;
	}
	
	void Update () 
	{
		movement = transform.position;
		FloatThePlane ();

		Move ();

		SetPosition ();
		SetRotation ();
	}

	void Move()
	{
		MoveHorizontally ();
		MoveForward ();
	}

	void SetPosition()
	{
		UpdatePosition ();
		ClampPosition ();
	}

	void FloatThePlane()
	{
		movement.y = Mathf.Sin (Time.time * floatingSpeed) * floatingHeight;
	}

	void MoveForward()
	{
		movement.z += forwardSpeed * Time.deltaTime;
	}

	void MoveHorizontally()
	{
		if (InputManager.Instance.hasInput) {
			if (InputManager.Instance.input.x < Screen.width / 2) {
				MoveOnLeft ();	

			} else {
				MoveOnRight ();	
			}
		} else {
			ResetRotation ();
		}
		/*
		if (Input.GetKey (KeyCode.D)) {
			MoveOnLeft ();	
		} else if (Input.GetKey (KeyCode.A)) {
			MoveOnRight ();	
		} else {
			zRotation = Mathf.MoveTowards (zRotation, 0, maxDelta);
		}
		*/
	}

	public void ResetRotation()
	{
		zRotation = Mathf.MoveTowards (zRotation, 0, rotationSpeed * Time.deltaTime);
	}

	void UpdatePosition()
	{
		transform.position = movement;
	}

	void SetRotation()
	{
		transform.eulerAngles = new Vector3 (0,0, zRotation);
	}

	void ClampPosition()
	{
		clampPos = transform.position;
		clampPos.x = Mathf.Clamp (transform.position.x, boundry.minX, boundry.maxX);
		
		zRotation = Mathf.Clamp (zRotation, -tiltAngle, tiltAngle);
		transform.position = clampPos;
	}

	public void MoveOnRight()
	{
		movement.x += movementSpeed * Time.deltaTime;
		zRotation -= Time.deltaTime * rotationSpeed;
	}

	public void MoveOnLeft()
	{
		movement.x -= movementSpeed * Time.deltaTime;
		zRotation += Time.deltaTime * rotationSpeed;
	}

}
