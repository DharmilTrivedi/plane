﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTest : MonoBehaviour {

	public Transform target;
	public float speed;

	void FixedUpdate()
	{
		target.Rotate (Vector3.right, Time.deltaTime * speed);
	}
}
