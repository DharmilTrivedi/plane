﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SplashView : BaseView {

	public Image slider;

	void Start () {
		StartCoroutine (wait ());
	}

	IEnumerator wait()
	{
		float i = 0;
		float rate = 1 / 3f;
		while (i < 1) 
		{
			i += Time.deltaTime * rate;
			yield return 0;
			slider.fillAmount = i;
		}

		UIManager.Instance.homeView.ShowView ();
		HideView ();
	}
}
