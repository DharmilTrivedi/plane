﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseView : MonoBehaviour {

	#region PUBLIC_REFERANCES
	#endregion

	#region PRIVATE_REFERANCES
	#endregion

	#region UNITY_CALLBACKS
	#endregion

	#region PUBLIC_METHODS

	public virtual void HideView(){
		this.gameObject.SetActive (false);
	}

	public virtual void ShowView(){
		this.gameObject.SetActive (true);
	}

	#endregion

	#region PRIVATE_METHODS
	#endregion

	#region COROUTINES
	#endregion
}
