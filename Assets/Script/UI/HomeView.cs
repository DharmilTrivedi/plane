﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HomeView : BaseView {

	#region PUBLIC_REFERANCES
	#endregion

	#region PRIVATE_REFERANCES
	#endregion

	#region UNITY_CALLBACKS
	#endregion

	#region PUBLIC_METHODS
	public override void ShowView ()
	{
		base.ShowView ();
		GameManager.Instance.gameState = GameState.GAME_SET;
		EventManager.RaiseOnGameSet ();
	}

	public void OnPlayButtone(){
		//Debug.Log ("j");
		GameManager.Instance.OnGameStart ();
		UIManager.Instance.gamePlayView.ShowView ();
		HideView ();
	}

	#endregion

	#region PRIVATE_METHODS
	#endregion

	#region COROUTINES
	#endregion
}
